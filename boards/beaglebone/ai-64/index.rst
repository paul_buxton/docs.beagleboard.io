.. _bbai64-home:

BeagleBone AI-64
###################

BeagleBone® AI-64 brings a complete system for developing artificial intelligence (AI) and machine learning
solutions with the convenience and expandability of the BeagleBone® platform and the peripherals on board to
get started right away learning and building applications. With locally hosted, ready-to-use, open-source
focused tool chains and development environment, a simple web browser, power source and network connection
are all that need to be added to start building performance-optimized embedded applications. Industry-leading
expansion possibilities are enabled through familiar BeagleBone® cape headers, with hundreds of open-source
hardware examples and dozens of readily available embedded expansion options available off-the-shelf.

.. grid:: 2

    .. grid-item::
        :columns: 12 12 12 4

        .. figure:: media/OSHW_mark_US002120.*
            :width: 200
            :target: https://certification.oshwa.org/us002120.html
            :alt: BeagleBone AI-64 OSHW Mark

    .. grid-item::
        :columns: 12 12 12 8

        .. admonition:: License Terms

            * This work is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__
            * Design materials and license can be found in the `git repository <https://git.beagleboard.org/beagleboard/beaglebone-ai-64>`__
            * Use of the boards or design materials constitutes an agreement to the :ref:`boards-terms-and-conditions`
            * Software images and purchase links available on the `board page <https://www.beagleboard.org/boards/beaglebone-ai-64>`__
            * For export, emissions and other compliance, see :ref:`beaglebone-ai-64-support-information`

.. image:: media/bbai64-45-front.jpg
   :width: 400px
   :align: center
   :alt: Fig: BeagleBone AI-64 front at 45° angle

.. toctree::
   :maxdepth: 1

   01-introduction
   02-quick-start
   03-design-and-specifications
   04-connectors-and-pinouts
   05-demos-and-tutorials
   06-support
